# 基于Gradio YOLOv5 Det v0.3 HuggingFace Demo的自定义接口脚本
# 创建人：曾逸夫
# 创建时间：2022-05-29

import os

os.system("pip install gradio==3.0.6")

import gradio as gr

# 描述
description = "<div align='center'>可自定义目标检测模型、安装简单、使用方便</div>"

# 示例图片
examples = [
    [
        "./img_examples/bus.jpg",
        "cpu",
        "yolov5s",
        640,
        0.6,
        0.5,
        10,
        ["人", "公交车"],
        ["label", "pdf"],],
    [
        "./img_examples/giraffe.jpg",
        "cpu",
        "yolov5l",
        320,
        0.5,
        0.45,
        12,
        ["长颈鹿"],
        ["label", "pdf"],],]

demo = gr.Interface.load(name="spaces/Zengyf-CVer/Gradio_YOLOv5_Det_v3", description=description, examples=examples)

demo.launch()
